Agregar el archivo config.js
- creamos la carpeta config dentro del proyecto sesion5_gitflow si no exite.
- en la carpeta config creamos el archivo config.js
- una vez creado insertamos el siguiente codigo:

------- inicio script ---------
'use strict';

module.exports = {
    production: false,
    usernameDefault: "TALLER_GIT_1",
    app: {
      port: 3001
    },
};


------- final script ----------


- realizamos el commit y publicamos el cambio.